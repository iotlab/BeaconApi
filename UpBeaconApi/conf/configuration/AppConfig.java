package configuration;

import com.vng.jcore.common.Config;

public class AppConfig {

    public static String mBeaconHost;
    public static int mBeaconPort;
    public static String mBeaconAppHost;
    public static int mBeaconAppPort;
    public static String mBeaconPartnerHost;
    public static int mBeaconPartnerPort;
    public static String mBeacon2PartnerListHost;
    public static int mBeacon2PartnerListPort;
    public static String mPartner2BeaconListHost;
    public static int mPartner2BeaconListPort;
    public static String mString2ListStringHost;
    public static int mString2ListStringPort;
    public static String mGeneratorHost;
    public static int mGeneratorPort;
    public static String mBeaconName2IdHost;
    public static int mBeaconName2IdPort;
    public static long mBeaconBossId = 1;
    public static long mAppBossId = 2;
    public static long mPartnerBossId = 3;
    public static long mAdvertisementBossId = 4;
    public static String mBeaconIdListHost;
    public static int mBeaconIdListPort;
    public static String mBeaconLocationHost;
    public static int mBeaconLocationPort;
    public static String mAdvertisementHost;
    public static int mAdvertisementPort;
    public static String mBeacon2AdListHost;
    public static int mBeacon2AdListPort;
    public static String mSessionForPartnerHost;
    public static int mSessionForPartnerPort;
    public static String mPartnerName2IdHost;
    public static int mPartnerName2IdPort;
    public static String mPartner2AdListHost;
    public static int mPartner2AdListPort;
    public static String mAd2BeaconListHost;
    public static int mAd2BeaconListPort;
    public static String mUserLocationHost;
    public static int mUserLocationPort;
    public static String mDevice2IdHost;
    public static int mDevice2IdPort;
    public static String mBeacon2DeviceViewAdListHost;
    public static int mBeacon2DeviceViewAdListPort;
    
    public static String mAppName = "BeaconFramework";
    public static String mKeyHash = "fesrirs1ks2";
    public static String mKeyHash2 = "iriekjks21sk";
    public static boolean mIsGenBeaconName = false;
    public static boolean mIsGenParterName = false;
    public static boolean mIsGenAppName = false;
    public static boolean mIsGenAdvertisementName = false;
    public static boolean mIsGenDeviceIdName = false;
    public static String mGenBeaconName = "UpBeacon";
    public static String mGenParterName = "UpBeaconPartner";
    public static String mGenAppName = "UpBeaconApp";
    public static String mGenAdvertisementName = "UpBeaconAdvertisement";
    public static String mGenDeviceIdName = "UpBeaconDeviceId";
    public static double USER_DISTANCE_RADIUS = 0.05; // 50 MET
    
//    public static String mProfileHost;
//    public static int mProfilePort;
//    public static String mPassportHost;
//    public static int mPassportPort;
//    public static String mSessionHost;
//    public static int mSessionPort;
//    public static String mName2IdHost;
//    public static int mName2IdPort;
//    public static String mEmail2IdHost;
//    public static int mEmail2IdPort;
//    public static String mPhone2IdHost;
//    public static int mPhone2IdPort;
//    public static String mId2PhoneHost;
//    public static int mId2PhonePort;
//    public static String mId2SessionListHost;
//    public static int mId2SessionListPort;
    

//    public static String mCodeActiveHost;
//    public static int mCodeActivePort;
//    public static String mLimitPhoneActiveHost;
//    public static int mLimitPhoneActivePort;
//    public static String mSendSMSHost;
//    public static int mSendSMSPort;
//    public static int MAX_GET_CODE_NUMBER_IN_DAY = 3;
//    public static long TIME_RESET = 24 * 60 * 60;
//    public static long LIVE_TIME_CODE_ACTIVE = 86400;
//    public static String mSMSContent = "Ma so kich hoat VietChat cua ban la: ";

    public static int PORT_LISTEN;
    public static String API_CONTROLLER_PATH;
    public static String OPEN_API_CONTROLLER_PATH;
    public static String OPEN_API_TEST_CONTROLLER_PATH;
    public static String OPEN_API_CONTROLLER_4Partner;
    public static String MOBILEMANAGER_HOST;
    public static int MOBILEMANAGER_PORT;
    public static String OAUTH_HOST;
    public static int OAUTH_PORT;
    public static String SCRIBER_LOG_HOST;
    public static int SCRIBER_LOG_PORT;
    public static int N_BACKGROUND_THREAD;
    public static String BASE_URL_STRING;
    public static String OPEN_ADS_PATH;
    //static 

    static {
        try {
            PORT_LISTEN = getIntParam("base_setting", "port_listen");
            API_CONTROLLER_PATH = getStrParam("base_setting", "api_controller_path");
            OPEN_API_CONTROLLER_PATH = getStrParam("base_setting", "open_api_controller_path");
            OPEN_API_TEST_CONTROLLER_PATH = getStrParam("base_setting", "open_api_test_controller_path");
            OPEN_API_CONTROLLER_4Partner = getStrParam("base_setting", "open_api_controller_4_partner");
                    
            BASE_URL_STRING = getStrParam("base_setting", "base_url_string");
            OPEN_ADS_PATH = getStrParam("base_setting", "open_ads_path");
            
            mBeaconHost = AppConfig.getStrParam("beacon", "host", "10.30.56.29");
            mBeaconPort = AppConfig.getIntParam("beacon", "port", 7101);
            mBeaconAppHost = getStrParam("beaconapp", "host");
            mBeaconAppPort = getIntParam("beaconapp", "port");
            mBeaconPartnerHost = getStrParam("beaconpartner", "host");
            mBeaconPartnerPort = getIntParam("beaconpartner", "port");
            mBeacon2PartnerListHost = getStrParam("beacon2partnerlist", "host");
            mBeacon2PartnerListPort = getIntParam("beacon2partnerlist", "port");
            mPartner2BeaconListHost = getStrParam("partner2beaconlist", "host");
            mPartner2BeaconListPort = getIntParam("partner2beaconlist", "port");
            mGeneratorHost = getStrParam("generator", "host");
            mGeneratorPort = getIntParam("generator", "port");
            mBeaconName2IdHost = getStrParam("beaconname2id", "host");
            mBeaconName2IdPort = getIntParam("beaconname2id", "port");
            mBeaconIdListHost = getStrParam("beaconidlist", "host");
            mBeaconIdListPort = getIntParam("beaconidlist", "port");
            mBeaconLocationHost = getStrParam("beaconlocationsearch", "host");
            mBeaconLocationPort = getIntParam("beaconlocationsearch", "port");
            mAdvertisementHost = getStrParam("beaconadvertisement", "host");
            mAdvertisementPort = getIntParam("beaconadvertisement", "port");
            mBeacon2AdListHost = getStrParam("beacon2advertisementlist", "host");
            mBeacon2AdListPort = getIntParam("beacon2advertisementlist", "port");
            mSessionForPartnerHost = getStrParam("session4partner", "host");
            mSessionForPartnerPort = getIntParam("session4partner", "port");
            mPartnerName2IdHost = getStrParam("partnername2id", "host");
            mPartnerName2IdPort = getIntParam("partnername2id", "port");
            mPartner2AdListHost = getStrParam("partner2advertisementlist", "host");
            mPartner2AdListPort = getIntParam("partner2advertisementlist", "port");
            mUserLocationHost = getStrParam("devicelocationsearch", "host");
            mUserLocationPort = getIntParam("devicelocationsearch", "port");
            mDevice2IdHost = getStrParam("device2id", "host");
            mDevice2IdPort = getIntParam("device2id", "port");
            mBeacon2DeviceViewAdListHost = getStrParam("beacon2deviceviewad", "host");
            mBeacon2DeviceViewAdListPort = getIntParam("beacon2deviceviewad", "port");
            

            mAppName = getStrParam("base_setting", "appname");
            mKeyHash = getStrParam("base_setting", "keyhash1");
            mKeyHash2 = getStrParam("base_setting", "keyhash2");

//            mProfileHost = AppConfig.getStrParam("userprofile", "host", "10.30.56.29");
//            mProfilePort = AppConfig.getIntParam("userprofile", "port", 7101);
//            mPassportHost = getStrParam("snspassport", "host");
//            mPassportPort = getIntParam("snspassport", "port");
//            mSessionHost = getStrParam("snssession", "host");
//            mSessionPort = getIntParam("snssession", "port");
//            mName2IdHost = getStrParam("snsnametoid", "host");
//            mName2IdPort = getIntParam("snsnametoid", "port");
//            mEmail2IdHost = getStrParam("emailtoid", "host");
//            mEmail2IdPort = getIntParam("emailtoid", "port");
//            mSendSMSHost = getStrParam("sendsms", "host");
//            mSendSMSPort = getIntParam("sendsms", "port");
//            mSMSContent = getStrParam("sendsms", "smscontent");
//            mCodeActiveHost = getStrParam("codeactive", "host");
//            mCodeActivePort = getIntParam("codeactive", "port");
//            mLimitPhoneActiveHost = getStrParam("limitcodeactive", "host");
//            mLimitPhoneActivePort = getIntParam("limitcodeactive", "port");
//            mPhone2IdHost = getStrParam("phonetoid", "host");
//            mPhone2IdPort = getIntParam("phonetoid", "port");
//            mId2PhoneHost = getStrParam("idtophone", "host");
//            mId2PhonePort = getIntParam("idtophone", "port");
//            mId2SessionListHost = getStrParam("id2sessionlist", "host");
//            mId2SessionListPort = getIntParam("id2sessionlist", "port");

            N_BACKGROUND_THREAD = 4;

            OAUTH_HOST = getStrParam("oauth", "host");
            OAUTH_PORT = getIntParam("oauth", "port");

            SCRIBER_LOG_HOST = getStrParam("scriber_log", "host");
            SCRIBER_LOG_PORT = getIntParam("scriber_log", "port");

        } catch (Exception e) {
        }
    }

    public static String getStrParam(String section, String name) {
//        String ret = Config.getParam(section, name);
//        if (ret == null) {
//            System.out.println("get String param " + ret + " section:" + section + "  " + name);
//            return "";
//        }
//        return ret;
        return getStrParam(section, name, "");
    }

    public static String getStrParam(String section, String name, String strDef) {
        String ret = Config.getParam(section, name);
        if (ret == null) {
            System.out.println("get String param " + ret + " section:" + section + "  " + name);
            return strDef;
        }
        return ret;
    }

    public static int getIntParam(String section, String name) {
//        String ret = getStrParam(section, name);
//        if (ret.equals("")) {
//            System.out.println("get int param " + ret + " section:" + section + " " + name);
//            return 0;
//        }
//        Integer retInt = Integer.valueOf(ret);
//        if (retInt == null) {
//            return 0;
//        }
//        return retInt;
        return getIntParam(section, name, 0);
    }

    public static int getIntParam(String section, String name, int intDef) {
        String ret = getStrParam(section, name);
        if (ret.equals("")) {
            System.out.println("get int param " + ret + " section:" + section + " " + name);
            return intDef;
        }
        Integer retInt = Integer.valueOf(ret);
        if (retInt == null) {
            return intDef;
        }
        return retInt;
    }
}
