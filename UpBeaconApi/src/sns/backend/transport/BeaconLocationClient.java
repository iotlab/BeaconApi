/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sns.backend.transport;

import configuration.AppConfig;
import java.util.List;
import jcommon.transport.client.ClientFactory;
import jcommon.transport.client.TClientInfo;

/**
 *
 * @author prophet
 */
public class BeaconLocationClient {

    private static final BeaconLocationClient m_instance = new BeaconLocationClient();

    public static BeaconLocationClient instance() {
        return m_instance;
    }

    public TClientInfo getClientInfo() {
        TClientInfo aInfo = ClientFactory.getClient(
                AppConfig.mBeaconLocationHost, AppConfig.mBeaconLocationPort, vng.up.beacon.location.thrift.TLocationBasedUserSearchService.Client.class, org.apache.thrift.protocol.TCompactProtocol.class
        );
        if (aInfo != null) {
            aInfo.sureOpen();
        }
        return aInfo;
    }

    public boolean add(double latitude, double longitude, long beaconId) {
        TClientInfo aInfo = getClientInfo();
        boolean _return = false;
        vng.up.beacon.location.thrift.TLocationBasedUserSearchService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.add(latitude, longitude, beaconId);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.add(latitude, longitude, beaconId);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }

    public List<Long> searchUsers(double latitude, double longitude, double distance, int idxStart, int size) {
        TClientInfo aInfo = getClientInfo();
        List<Long> _return = null;
        vng.up.beacon.location.thrift.TLocationBasedUserSearchService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.searchUsers(latitude, longitude, distance, idxStart, size);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.searchUsers(latitude, longitude, distance, idxStart, size);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }

    public List<vng.up.beacon.location.thrift.TUserDistance> searchUsersWithDistance(double latitude, double longitude, double distance, int idxStart, int size) {
        TClientInfo aInfo = getClientInfo();
        List<vng.up.beacon.location.thrift.TUserDistance> _return = null;
        vng.up.beacon.location.thrift.TLocationBasedUserSearchService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.searchUsersWithDistance(latitude, longitude, distance, idxStart, size);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.searchUsersWithDistance(latitude, longitude, distance, idxStart, size);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }

    public boolean setWaitingTime(int waitingTime) {
        TClientInfo aInfo = getClientInfo();
        boolean _return = false;
        vng.up.beacon.location.thrift.TLocationBasedUserSearchService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.setWaitingTime(waitingTime);
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.setWaitingTime(waitingTime);

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }

    public int getSize() {
        TClientInfo aInfo = getClientInfo();
        int _return = -1;
        vng.up.beacon.location.thrift.TLocationBasedUserSearchService.Client aClient = aInfo.getClientT();
        try {
            _return = aClient.getSize();
        } catch (Exception e) {
            try {
                aInfo.close();
                aClient = aInfo.getClientT();
                _return = aClient.getSize();

            } catch (Exception e2) {
            }

        }
        aInfo.cleanUp();
        return _return;
    }
}
