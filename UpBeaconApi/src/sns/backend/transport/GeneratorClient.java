/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sns.backend.transport;

import configuration.AppConfig;
import jcommon.transport.client.ClientFactory;
import jcommon.transport.client.TClientInfo;

/**
 *
 * @author prophet
 */
public class GeneratorClient {
    private static final GeneratorClient m_instance = new GeneratorClient();

    public static GeneratorClient instance() {
        return m_instance;
    }

    // Need to overide this method for diferent purpose
    TClientInfo getClientInfo() {
        return ClientFactory.getClient(
                AppConfig.mGeneratorHost, AppConfig.mGeneratorPort,
                ZGenerator.Generator.Client.class,
                org.apache.thrift.protocol.TCompactProtocol.class);
    }
    
    public int createGenerator(String genName) {
        int _return = -1;
        TClientInfo aInfo = getClientInfo();
        ZGenerator.Generator.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.createGenerator(genName);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.createGenerator(genName);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }
    
    public long getValue(String genName) {
        long _return = -1;
        TClientInfo aInfo = getClientInfo();
        ZGenerator.Generator.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.getValue(genName);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.getValue(genName);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }
}
