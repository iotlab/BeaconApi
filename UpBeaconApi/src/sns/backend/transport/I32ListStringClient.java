/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sns.backend.transport;

import configuration.AppConfig;
import java.util.ArrayList;
import java.util.List;
import jcommon.transport.client.ClientFactory;
import jcommon.transport.client.TClientInfo;

/**
 *
 * @author prophet
 */
public class I32ListStringClient {
    private static final I32ListStringClient m_instance = new I32ListStringClient();

    public static I32ListStringClient instance() {
        return m_instance;
    }

    // Need to overide this method for diferent purpose
    public TClientInfo getClientInfo() {
        //System.out.println("host: " + AppConfig.mId2SessionListHost + " port: " + AppConfig.mId2SessionListPort);
        
        String host = "127.0.0.1";
        int port = 9010;
        return ClientFactory.getClient(
                host, port,
                vng.up.core.list.i32string.I32ListStringService.Client.class,
                org.apache.thrift.protocol.TBinaryProtocol.class);
    }
    
    public List<String> getSlice(int key, int start, int length){
        List<String> _return = new ArrayList<String>();
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.i32string.I32ListStringService.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.getSlice(key, start, length);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.getSlice(key, start, length);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }
    
    public List<String> getListAll(int key){
        List<String> _return = new ArrayList<String>();
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.i32string.I32ListStringService.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.getListAll(key);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.getListAll(key);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }
    
    public void clearData(int key){
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.i32string.I32ListStringService.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                aClient.clearData(key);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        aClient.clearData(key);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
    }
    
    public boolean remove(int key, String entry){
        boolean _return = false;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.i32string.I32ListStringService.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.remove(key, entry);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.remove(key, entry);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }
        return _return;
    }
    
    public boolean removeList(int key, List<String> entryList){
        boolean _return = false;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.i32string.I32ListStringService.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.removeList(key, entryList);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.removeList(key, entryList);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }
        return _return;
    }
    
    public boolean put(int key, String entry){
        boolean _return = false;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.i32string.I32ListStringService.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.put(key, entry);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.put(key, entry);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }    
        return _return;
    }
    
    public boolean putList(int key, List<String> entryList){
        boolean _return = false;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.i32string.I32ListStringService.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.putList(key, entryList);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.putList(key, entryList);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }    
        return _return;
    }
}
