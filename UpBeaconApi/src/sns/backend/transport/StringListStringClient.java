/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sns.backend.transport;

import configuration.AppConfig;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import jcommon.transport.client.ClientFactory;
import jcommon.transport.client.TClientInfo;
import vng.up.core.list.stringstring.TValue;

/**
 *
 * @author prophet
 */
public class StringListStringClient {

    private static final StringListStringClient m_instance = new StringListStringClient();

    public static StringListStringClient instance() {
        return m_instance;
    }

    // Need to overide this method for diferent purpose
    public TClientInfo getClientInfo() {
        System.out.println("host: " + AppConfig.mString2ListStringHost + " port: " + AppConfig.mString2ListStringPort);
        return ClientFactory.getClient(
                AppConfig.mString2ListStringHost, AppConfig.mString2ListStringPort,
                vng.up.core.list.stringstring.StringListStringService.Client.class,
                org.apache.thrift.protocol.TBinaryProtocol.class);
    }

    public int count(String key) {
        int _return = -1;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.stringstring.StringListStringService.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.count(key);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.count(key);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }
    
    public boolean existed(String key, String entry) {
        boolean _return = false;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.stringstring.StringListStringService.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.existed(key, entry);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.existed(key, entry);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    public List<String> getListAll(String key) {
        List<String> _return = null;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.stringstring.StringListStringService.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.getListAll(key);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.getListAll(key);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    public List<String> getSlice(String key, int start, int length) {
        List<String> _return = null;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.stringstring.StringListStringService.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.getSlice(key, start, length);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.getSlice(key, start, length);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    public List<String> getSliceFromEntry(String key, String entryStart, int length) {
        List<String> _return = null;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.stringstring.StringListStringService.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.getSliceFromEntry(key, entryStart, length);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.getSliceFromEntry(key, entryStart, length);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    public List<String> getSliceReverse(String key, int start, int length) {
        List<String> _return = null;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.stringstring.StringListStringService.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.getSliceReverse(key, start, length);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.getSliceReverse(key, start, length);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    public List<String> getSliceFromEntryReverse(String key, String entryStart, int length) {
        List<String> _return = null;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.stringstring.StringListStringService.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.getSliceFromEntryReverse(key, entryStart, length);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.getSliceFromEntryReverse(key, entryStart, length);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    
    public boolean put(String key, String entry) {
        boolean _return = false;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.stringstring.StringListStringService.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.put(key, entry);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.put(key, entry);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    /**
     * Clear old data, put new value on the key
     *
     * @param key
     * @param value
     */
    public boolean putValue(String key, TValue value) {
        boolean _return = false;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.stringstring.StringListStringService.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.putValue(key, value);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.putValue(key, value);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    public boolean putList(String key, List<String> entryList) {
        boolean _return = false;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.stringstring.StringListStringService.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.putList(key, entryList);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.putList(key, entryList);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    public boolean insert(String key, String entry) {
        boolean _return = false;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.stringstring.StringListStringService.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.insert(key, entry);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.insert(key, entry);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    public boolean insertAt(String key, String entry, int index) {
        boolean _return = false;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.stringstring.StringListStringService.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.insertAt(key, entry, index);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.insertAt(key, entry, index);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    public boolean remove(String key, String entry) {
        boolean _return = false;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.stringstring.StringListStringService.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.remove(key, entry);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.remove(key, entry);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    public boolean removeAt(String key, int index) {
        boolean _return = false;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.stringstring.StringListStringService.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.removeAt(key, index);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.removeAt(key, index);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    /**
     * Make an entry the first in unsorted list and return true if it existed,
     * do nothing on sorted list return false
     *
     * @param key
     * @param entry
     */
    public boolean bumpUp(String key, String entry) {
        boolean _return = false;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.stringstring.StringListStringService.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.bumpUp(key, entry);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.bumpUp(key, entry);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    /**
     * Remove entry list
     *
     * @param key
     * @param entryList
     */
    public boolean removeList(String key, List<String> entryList) {
        boolean _return = false;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.stringstring.StringListStringService.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.removeList(key, entryList);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.removeList(key, entryList);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    public void clearData(String key) {
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.stringstring.StringListStringService.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                aClient.clearData(key);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        aClient.clearData(key);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
    }

    public Map<String, Integer> multiCount(List<String> keyList) {
        Map<String, Integer> _return = null;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.stringstring.StringListStringService.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                _return = aClient.multiCount(keyList);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.multiCount(keyList);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    public Map<String, Boolean> multiExisted(List<String> keyList, String entry) {
        Map<String, Boolean> _return = null;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.stringstring.StringListStringService.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                    _return = aClient.multiExisted(keyList, entry);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.multiExisted(keyList, entry);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    public Map<String, List<String>> multiGetListAll(List<String> keyList) {
        Map<String, List<String>> _return = null;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.stringstring.StringListStringService.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                    _return = aClient.multiGetListAll(keyList);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.multiGetListAll(keyList);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    public Map<String, List<String>> multiGetSlice(List<String> keyList, int start, int length) {
        Map<String, List<String>> _return = null;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.stringstring.StringListStringService.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                    _return = aClient.multiGetSlice(keyList, start, length);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.multiGetSlice(keyList, start, length);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    public Map<String, List<String>> multiGetSliceReverse(List<String> keyList, int start, int length) {
        Map<String, List<String>> _return = null;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.stringstring.StringListStringService.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                    _return = aClient.multiGetSliceReverse(keyList, start, length);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.multiGetSliceReverse(keyList, start, length);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }

    /**
     * Put same entry to multiple keys
     *
     * @param keyList
     * @param entry
     */
    public Map<String, Boolean> multiPut(List<String> keyList, String entry) {
        Map<String, Boolean> _return = null;
        TClientInfo aInfo = getClientInfo();
        vng.up.core.list.stringstring.StringListStringService.Client aClient =
                aInfo.getClientT();

        if (aClient != null) {
            try {
                    _return = aClient.multiPut(keyList, entry);
            } catch (Exception e) {
                aInfo.close();
                aInfo.doOpen();
                aClient = aInfo.getClientT();
                try {
                    if (aClient != null)
                        _return = aClient.multiPut(keyList, entry);
                } catch (Exception e2) {
                }
            }
            aInfo.cleanUp();
        }
        else {
            aInfo.cleanUp();
        }     
        return _return;
    }
}
