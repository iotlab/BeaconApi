/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservlet;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import service.models.AdvertisementModel;
import service.models.BeaconModel;
import service.models.ForPartnerModel;
import service.response.DataResponse;
import service.utils.ServletUtil;

/**
 *
 * @author prophet
 */
public class PartnerController extends BaseServer {

    private static final Logger log = Logger.getLogger(PartnerController.class);

    @Override
    protected void doProcess(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
        //LogData logEntry = new LogData(request, LogConstants.CATEGORY_MAPI_API_LOGIN);
        resp.addHeader("Access-Control-Allow-Origin", "*");
//        resp.addHeader("Access-Control-Allow-Origin:", "*");
//        resp.addHeader("Access-Control-Allow-Origin: *", "*");
        DataResponse dataResponse = null;
        String method = ServletUtil.getStringParameter(request, "method");
        if (method != null) {
            method = method.replace(".", "_").toLowerCase();
        }
        if ("partner_login".equals(method)) {
            dataResponse = doLogin(request);
            sendResponseData(dataResponse, resp);
            return;
        }

        int userId = checkSession(request);
        if (userId < 0) {
            sendResponseData(DataResponse.SESSION_EXPIRE, resp);
            return;
        }

        System.out.println("method : " + method);
        if ("partner_getbeacons".equals(method)) {
            dataResponse = doGetBeaconFromPartner(request, userId);
        } else if ("beacon_advertise_addads".equals(method)) {
            dataResponse = doAddAdvertisement(request, userId);
        } else if ("partner_getadlist".equals(method)) {
            dataResponse = doGetAdsInfoList(request, userId);
        } else if ("partner_assigorremoveadvertis".equals(method)) {
            dataResponse = doAssignOrRemoveAdvertisToBeacon(request, userId);
        } else if ("partner_beaconadvertis".equals(method)) {
            dataResponse = doGetListAdvertisOfBeacon(request, userId);
        } else if ("partner_remove_advertisement".equals(method)) {
            dataResponse = doRemoveAdvertisement(request, userId);
        } else if ("partner_advertise_edit".equals(method)) {
            dataResponse = doEditAdvertisement(request, userId);
        } else if ("partner_put".equals(method)) {
            dataResponse = doPutBeacon(request, userId);
        } else if ("partner_remove_beacon".equals(method)){
            dataResponse = doRemoveBeacon(request, userId);
        }
        sendResponseData(dataResponse, resp);
    }

    private void sendResponseData(DataResponse dataResponse, HttpServletResponse resp) {
        if (dataResponse != null) {
            dataResponse.setEscape(true);
            out(dataResponse.toString(), resp);
        }
    }

    private int checkSession(HttpServletRequest request) {
        String session = ServletUtil.getStringParameter(request, "sessionId");
        if (session == null || session.length() == 0) {
            log.info("*** missing session param");
            return -1;
        }
        return ForPartnerModel.getPartnerId(session);
    }

    private DataResponse doLogin(HttpServletRequest request) {
        if (!checkValidParam(request, new String[]{"username", "password"})) {
            return DataResponse.MISSING_PARAM;
        }
        String username = ServletUtil.getStringParameter(request, "username");
        String password = ServletUtil.getStringParameter(request, "password");
        return ForPartnerModel.login(username, password);
    }

    private DataResponse doGetBeaconFromPartner(HttpServletRequest request, int userId) {
        System.out.println("doGetBeaconFromPartner");
        if (!checkValidParam(request, new String[]{"start", "length"})) {
            return DataResponse.MISSING_PARAM;
        }

        int start = ServletUtil.getIntParameter(request, "start");
        int length = ServletUtil.getIntParameter(request, "length");
        return ForPartnerModel.getBeaconListFromPartner(userId, start, length);
    }

    private DataResponse doAddAdvertisement(HttpServletRequest request, int userId) {
        if (!checkValidParam(request, new String[]{"title", "content"})) {
            return DataResponse.MISSING_PARAM;
        }
        String title = ServletUtil.getStringParameter(request, "title");
        String content = ServletUtil.getStringParameter(request, "content");
        long startTime = 0;
        long endTime = 0;
        try {
            String startTimeString = ServletUtil.getStringParameter(request, "startTime");
            String endTimeString = ServletUtil.getStringParameter(request, "endTime");

            Date date;
            date = new SimpleDateFormat("dd/MM/yyyy").parse(startTimeString);
            startTime = date.getTime();

            date = new SimpleDateFormat("dd/MM/yyyy").parse(endTimeString);
            endTime = date.getTime();
        } catch (ParseException ex) {
        }
        return ForPartnerModel.addAdvertisement(userId, title, content, startTime, endTime);
    }

    private DataResponse doGetAdsInfoList(HttpServletRequest request, int userId) {
        System.out.println("doGetAdsInfoList");
        if (!checkValidParam(request, new String[]{"start", "length"})) {
            return DataResponse.MISSING_PARAM;
        }

        int start = ServletUtil.getIntParameter(request, "start");
        int length = ServletUtil.getIntParameter(request, "length");
        return ForPartnerModel.getAdvertisementInfoList(userId, start, length);
    }

    private DataResponse doAssignOrRemoveAdvertisToBeacon(HttpServletRequest request, int userId) {
        if (!checkValidParam(request, new String[]{"advertisId", "beaconId", "action"})) {
            return DataResponse.MISSING_PARAM;
        }
        long advertisId = ServletUtil.getLongParameter(request, "advertisId");
        long beaconId = ServletUtil.getLongParameter(request, "beaconId");

        int action = ServletUtil.getIntParameter(request, "action");
        if (action > 0) {
            return ForPartnerModel.assignAdvertisToBeacon(userId, beaconId, advertisId);
        }
        return ForPartnerModel.removeAdvertisAssignToBeacon(userId, beaconId, advertisId);
    }

    private DataResponse doGetListAdvertisOfBeacon(HttpServletRequest request, int partnerId) {
        System.out.println("doGetListAdvertisOfBeacon");
        if (!checkValidParam(request, new String[]{"beaconId"})) {
            return DataResponse.MISSING_PARAM;
        }
        long beaconId = ServletUtil.getLongParameter(request, "beaconId");
        return ForPartnerModel.getListAdvertisOfBeacon(beaconId, partnerId);
    }

    private DataResponse doRemoveAdvertisement(HttpServletRequest request, int partnerId) {
        if (!checkValidParam(request, new String[]{"advertismentId"})) {
            return DataResponse.MISSING_PARAM;
        }
        long advertismentId = ServletUtil.getLongParameter(request, "advertismentId");
        return ForPartnerModel.removeAdvertisement(partnerId, advertismentId);
    }

    private DataResponse doEditAdvertisement(HttpServletRequest request, int partnerId) {
        if (!checkValidParam(request, new String[]{"adId", "title", "content"})) {
            return DataResponse.MISSING_PARAM;
        }
        String title = ServletUtil.getStringParameter(request, "title");
        String content = ServletUtil.getStringParameter(request, "content");
        long adId = ServletUtil.getLongParameter(request, "adId");

        long startTime = 0;
        long endTime = 0;
        try {
            String startTimeString = ServletUtil.getStringParameter(request, "startTime");
            String endTimeString = ServletUtil.getStringParameter(request, "endTime");

            Date date;
            date = new SimpleDateFormat("dd/MM/yyyy").parse(startTimeString);
            startTime = date.getTime();

            date = new SimpleDateFormat("dd/MM/yyyy").parse(endTimeString);
            endTime = date.getTime();
        } catch (ParseException ex) {
        }
        return ForPartnerModel.editAdvertisement(partnerId, adId, title, content, startTime, endTime);

    }

    private DataResponse doPutBeacon(HttpServletRequest request, int partnerId) {
        System.out.println("doPutBeacon");
        if (!checkValidParam(request, new String[]{"uuid", "major", "minor", "lat", "lng", "enable", "name", "description"})) {
            return DataResponse.MISSING_PARAM;
        }
        String uuid = ServletUtil.getStringParameter(request, "uuid");
        int bMajor = ServletUtil.getIntParameter(request, "major");
        int bMinor = ServletUtil.getIntParameter(request, "minor");
        String latStr = ServletUtil.getStringParameter(request, "lat");
        String lngStr = ServletUtil.getStringParameter(request, "lng");
        int enable = ServletUtil.getIntParameter(request, "enable");
        String name = ServletUtil.getStringParameter(request, "name", "");
        String description = ServletUtil.getStringParameter(request, "description", "");
        double lat = Double.parseDouble(latStr);
        double lng = Double.parseDouble(lngStr);
        return ForPartnerModel.putBeaconInfo(uuid, bMajor, bMinor, lat, lng, (enable != 0), name, description,partnerId);
    }
    
      private DataResponse doRemoveBeacon(HttpServletRequest request, int userId) {
        System.out.println("doRemoveBeacon");
        if (checkValidParam(request, new String[]{"beaconId"})) {
            long beaconId = ServletUtil.getLongParameter(request, "beaconId");
            
            return ForPartnerModel.doRemoveBeacon(beaconId, userId);

        } else {
            return DataResponse.MISSING_PARAM;
        }
    }
    
}
