/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package webservlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import service.log.LogConstants;
//import service.log.LogRequestData;

public class BaseServer extends HttpServlet {

    private static Logger log = Logger.getLogger(BaseServer.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        try {
			//LogRequestData requestData = new LogRequestData(req, LogConstants.CATEGORY_MAPI_API_LOGIN_REQUEST);
            doProcess(req, resp);
			///requestData.sendLogApi();
        } catch (IOException ex) {
        } catch (ServletException ser) {
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        doGet(req, resp);
    }

    protected void doProcess(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
		log.info("do process data");
	}

    public String getZSessionFromCookie(HttpServletRequest request) {
        String zAuthString = null;
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie ck : cookies) {
                if (ck.getName().equalsIgnoreCase("zsession")) {
                    zAuthString = ck.getValue();
                    break;
                }
            }
        }
        return zAuthString;
    }

    public String getClientIP(HttpServletRequest request) {
        String clientIp = request.getHeader("X-FORWARDED-FOR");
        if (clientIp == null || clientIp.length() == 0) {
            clientIp = request.getHeader("X-Forwarded-For");
        }
        if (clientIp == null || clientIp.length() == 0) {
            clientIp = request.getHeader("x-forwarded-for");
        }
        if (clientIp == null || clientIp.length() == 0) {
            clientIp = request.getRemoteAddr();
        }
        return clientIp;
    }

    
    protected void out(String content, HttpServletResponse resp) {
        try {
            resp.setCharacterEncoding("utf-8");
            resp.addHeader("Content-Type", "application/json; charset=utf-8");
            PrintWriter out = resp.getWriter();
            out.print(content);
            out.close();
        } catch (Exception e) {
            log.error("Exception: " + e.getMessage());
        }

    }

    public boolean checkValidParam(HttpServletRequest request, String[] params) {
        try {
            Set<String> s = new HashSet<String>();
            Enumeration<String> requestParam = request.getParameterNames();

            while (requestParam.hasMoreElements()) {
                String param = requestParam.nextElement();
                s.add(param.toLowerCase());
            }

            for (int i = 0; i < params.length; i++) {
                if (!s.contains(params[i].toLowerCase())) {
                    return false;
                }
            }
        } catch (Exception e) {
            return false;
        }

        return true;
    }
    
}
