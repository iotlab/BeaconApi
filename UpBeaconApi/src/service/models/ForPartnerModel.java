/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.models;

import configuration.AppConfig;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.socialnetwork.snssession.TSessionInfoResult;
import service.models.data.AdvertisementInfoData;
import service.models.data.AdvertisementInfoDataList;
import service.models.data.BeaconInfoData;
import service.models.data.BeaconInfoDataList;
import service.models.data.PutBeaconData;
import service.response.DataResponse;
import sns.backend.transport.Ad2BeaconListClient;
import sns.backend.transport.Beacon2AdListClient;
import sns.backend.transport.Beacon2DeviceViewAdListClient;
import sns.backend.transport.Beacon2ParterListClient;
import sns.backend.transport.BeaconIdListClient;
import sns.backend.transport.BeaconLocationClient;
import sns.backend.transport.Partner2AdListClient;
import sns.backend.transport.Partner2BeaconListClient;
import sns.backend.transport.PartnerName2IdClient;
import sns.backend.transport.SNSSessionClient;
import sns.backend.transport.UpAdvertisementClient;
import sns.backend.transport.UpBeaconClient;
import sns.backend.transport.UpBeaconPartnerClient;
import sns.backend.transport.UserLocationClient;
import vng.up.beacon.thrift.TAdvertisementInfo;
import vng.up.beacon.thrift.TBeaconInfo;
import vng.up.beacon.thrift.TPartnerInfo;

/**
 *
 * @author prophet
 */
public class ForPartnerModel {

    public static DataResponse login(String username, String password) {
        long partnerId = PartnerName2IdClient.instance().get(username);
        if (partnerId <= 0) {
            return DataResponse.LOGIN_NOT_EXIST_USERNAME;
        }
        TPartnerInfo partnerInfo = UpBeaconPartnerClient.instance().get(partnerId);
        if (partnerInfo != null) {
            String storePw = partnerInfo.password;
            if (!password.equals(storePw)) {
                return DataResponse.LOGIN_WRONG_EMAIL_OR_PASSWORD;
            }
            int userId = (int) partnerId;
            String sessionKey = generateSession(AppConfig.mAppName, userId);
            SNSSessionClient.instance().addSession(sessionKey, partnerInfo.username, userId);
            Map<String, Object> dataMap = new HashMap<String, Object>();
            dataMap.put("userid", partnerId);
            dataMap.put("partnerId", partnerInfo.partnerId);
            dataMap.put("username", partnerInfo.username);
            dataMap.put("name", partnerInfo.name);
            dataMap.put("address", partnerInfo.address);
            dataMap.put("session", sessionKey);
            return new DataResponse(dataMap);
        }
        return DataResponse.PARAM_ERROR;
    }

    public static DataResponse getBeaconListFromPartner(int partnerId, int start, int length) {
//        long partnerId = getPartnerId(sessionKey);
//        if(partnerId <= 0){
//            return DataResponse.SESSION_EXPIRE;
//        }
        List<Long> beaconIdList = Partner2BeaconListClient.instance().getSlice(partnerId, start, length);
        if (beaconIdList != null) {
            List<TBeaconInfo> beaconInfoList = UpBeaconClient.instance().getBeaconInfoList(beaconIdList);
            if (beaconInfoList == null) {
                return DataResponse.PARAM_ERROR;
            } else {
                List<BeaconInfoData> modelDataList = new ArrayList<BeaconInfoData>();
                for (int i = 0; i < beaconInfoList.size(); i++) {
                    TBeaconInfo beaconInfo = beaconInfoList.get(i);
                    if (beaconInfo != null && beaconInfo.beaconId > 0) {
                        //List<Long> partnerList = Beacon2ParterListClient.instance().getListAll(beaconInfo.beaconId);
                        BeaconInfoData modelData = new BeaconInfoData(beaconInfo);
                        //modelData.partnerIds = partnerList;
                        //modelData.numberPartner = partnerList.size();
                        List<Long> nearUserList = UserLocationClient.instance().searchUsers(beaconInfo.lat, beaconInfo.lng, AppConfig.USER_DISTANCE_RADIUS, 0, 50);
                        if (nearUserList != null) {
                            modelData.nearUsers = nearUserList.size();
                        }
                        modelData.usersViewAd = Beacon2DeviceViewAdListClient.instance().count(beaconInfo.beaconId);
                        modelDataList.add(modelData);
                    }
                }
                int total = Partner2BeaconListClient.instance().count(partnerId);
                System.out.println("total: " + total);
                BeaconInfoDataList beaconInfoDataList = new BeaconInfoDataList(total, modelDataList);
                return new DataResponse(beaconInfoDataList);
            }
        }

        return DataResponse.PARAM_ERROR;
    }

    public static DataResponse addAdvertisement(int partnerId, String title, String content, long startTime, long endTime) {
        System.out.println("addAdvertisement title: " + title + " content: " + content + "startTime: " + startTime + " endTime: " + endTime);
        long adId = IdGeneratorModel.genAdvertisementId();
        if (adId <= 0) {
            System.out.println("adId: " + adId);
            return DataResponse.PARAM_ERROR;
        }

        TAdvertisementInfo adInfo = new TAdvertisementInfo(adId, title, content, startTime, endTime, true);
        boolean result = UpAdvertisementClient.instance().put(adInfo);
        if (result) {
            BeaconIdListClient.instance().put(AppConfig.mAdvertisementBossId, adId);
            Partner2AdListClient.instance().insert(partnerId, adId);
            AdvertisementInfoData adInfoData = new AdvertisementInfoData(adInfo);
            AdvertisementInfoData.AdvertisementInfoResponse addResponse = new AdvertisementInfoData.AdvertisementInfoResponse(result, adInfoData);
            return new DataResponse(addResponse);
        } else {
            AdvertisementInfoData.AdvertisementInfoResponse addResponse = new AdvertisementInfoData.AdvertisementInfoResponse(result, null);
            return new DataResponse(addResponse);
        }
    }

    public static DataResponse editAdvertisement(int partnerId, long adId, String title, String content, long startTime, long endTime) {
        if (adId <= 0) {
            System.out.println("adId: " + adId);
            return DataResponse.PARAM_ERROR;
        }
        if (!Partner2AdListClient.instance().existed(partnerId, adId)) {
            System.out.println("try to edit friend advertisement");
            return DataResponse.PARAM_ERROR;
        }
        TAdvertisementInfo adInfo = new TAdvertisementInfo(adId, title, content, startTime, endTime, true);
        return new DataResponse(UpAdvertisementClient.instance().put(adInfo));
    }

    public static DataResponse updateAdvertisement(String sessionKey, long adId, String title, String content, long startTime, long endTime, boolean enable) {
        long partnerId = getPartnerId(sessionKey);
        if (partnerId <= 0) {
            return DataResponse.SESSION_EXPIRE;
        }

        if (adId > 0) {
            TAdvertisementInfo adInfo = new TAdvertisementInfo(adId, title, content, startTime, endTime, enable);
            boolean result = UpAdvertisementClient.instance().put(adInfo);
            if (result) {
                AdvertisementInfoData adInfoData = new AdvertisementInfoData(adInfo);
                AdvertisementInfoData.AdvertisementInfoResponse addResponse = new AdvertisementInfoData.AdvertisementInfoResponse(result, adInfoData);
                return new DataResponse(addResponse);
            } else {
                AdvertisementInfoData.AdvertisementInfoResponse addResponse = new AdvertisementInfoData.AdvertisementInfoResponse(result, null);
                return new DataResponse(addResponse);
            }
        }
        return DataResponse.PARAM_ERROR;
    }

    public static DataResponse getAdvertisementInfoList(int partnerId, int start, int length) {
        List<Long> adIdList = Partner2AdListClient.instance().getSlice(partnerId, start, length);
        if (adIdList != null) {
            List<TAdvertisementInfo> adInfoList = UpAdvertisementClient.instance().getAdvertisementInfoList(adIdList);
            if (adInfoList == null) {
                return DataResponse.PARAM_ERROR;
            }
            List<AdvertisementInfoData> adModelList = new ArrayList<AdvertisementInfoData>();
            for (int i = 0; i < adInfoList.size(); i++) {
                TAdvertisementInfo adInfo = adInfoList.get(i);
                AdvertisementInfoData adInfoData = new AdvertisementInfoData(adInfo);
//                int numberBeacon = Partner2BeaconListClient.instance().count(partnerInfo.partnerId);
//                partnerInfoData.numberBeacon = numberBeacon;
                adModelList.add(adInfoData);
            }
            int total = Partner2AdListClient.instance().count(partnerId);
            AdvertisementInfoDataList returnList = new AdvertisementInfoDataList(total, adModelList);
            return new DataResponse(returnList);
        }
        return DataResponse.PARAM_ERROR;
    }

    public static int getPartnerId(String sessionKey) {
        TSessionInfoResult sessionResult = SNSSessionClient.instance().getSession(sessionKey);
        if (sessionResult == null) {
            return -1;
        }
        if (!sessionResult.isValid) {
            return -2;
        }
        if (sessionResult.session != null) {
            return sessionResult.session.uid;
        }
        return -3;
    }

    public static String generateSession(String KPrefix, int uid) {
        Date aDate = new Date();
        long aTimestamp = aDate.getTime() / 1000;
        String aTimeStr = String.valueOf(aTimestamp);
        String aUID = String.valueOf(uid);
        try {
            String aSession = ForPartnerModel.MD5(KPrefix + aUID + "." + aTimeStr);
            return aSession;
        } catch (NoSuchAlgorithmException ex1) {

        } catch (UnsupportedEncodingException ex1) {
        }
        return "";
    }

    private static String convertToHex(byte[] data) {
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i < data.length; i++) {
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                if ((0 <= halfbyte) && (halfbyte <= 9)) {
                    buf.append((char) ('0' + halfbyte));
                } else {
                    buf.append((char) ('a' + (halfbyte - 10)));
                }
                halfbyte = data[i] & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }

    public static String MD5(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md;
        md = MessageDigest.getInstance("MD5");
        //byte[] md5hash = new byte[32];
        md.update(text.getBytes("iso-8859-1"), 0, text.length());
        byte[] md5hash = md.digest();
        return convertToHex(md5hash);
    }

    public static DataResponse assignAdvertisToBeacon(int partnerId, long beaconId, long advertisId) {

        TAdvertisementInfo adInfo = UpAdvertisementClient.instance().get(advertisId);
        if (adInfo.advertisementId < 0) {
            return new DataResponse(false);
        }
        if (!Partner2AdListClient.instance().existed(partnerId, advertisId)) {
            return new DataResponse(false);
        }
        Ad2BeaconListClient.instance().put(advertisId, beaconId);
        Beacon2AdListClient.instance().put(beaconId, advertisId);
        return new DataResponse(true);
    }

    public static DataResponse removeAdvertisAssignToBeacon(int partnerId, long beaconId, long advertisId) {
        Beacon2AdListClient.instance().remove(beaconId, advertisId);
        Ad2BeaconListClient.instance().remove(advertisId, beaconId);
        return new DataResponse(true);
    }

    public static DataResponse getListAdvertisOfBeacon(long beaconId, int partnerId) {
        List<Long> allAdvertis = Partner2AdListClient.instance().getListAll(partnerId);
        List<Long> currentAdvertis = Beacon2AdListClient.instance().getListAll(beaconId);

        HashMap<String, Object> _returnMap = new HashMap<>();

        List<AdvertisementInfoData> remainAds = new ArrayList<>();
        List<AdvertisementInfoData> beaconAds = new ArrayList<>();
        _returnMap.put("remainAds", remainAds);
        _returnMap.put("currentAds", beaconAds);
        // System.out.println("all advertis " + allAdvertis);
        // System.out.println("currentAdvertis " + currentAdvertis);
        if (allAdvertis == null || allAdvertis.size() == 0) {
            return new DataResponse(_returnMap);
        }
        if (currentAdvertis != null && currentAdvertis.size() > 0) {
            allAdvertis.removeAll(currentAdvertis);
        }
        advertismentDataFromListId(allAdvertis, remainAds);
        advertismentDataFromListId(currentAdvertis, beaconAds);

        return new DataResponse(_returnMap);
    }

    private static void advertismentDataFromListId(List<Long> listAdvertisId, List<AdvertisementInfoData> result) {
        if (listAdvertisId == null || listAdvertisId.size() == 0) {
            return;
        }
        List<TAdvertisementInfo> allTAdvertis = UpAdvertisementClient.instance().getAdvertisementInfoList(listAdvertisId);
        if (allTAdvertis == null || allTAdvertis.size() == 0) {
            System.out.println("get advertismentData fail " + listAdvertisId);
            return;
        }
        for (int i = 0; i < allTAdvertis.size(); i++) {
            TAdvertisementInfo adInfo = allTAdvertis.get(i);
            if (adInfo.advertisementId <= 0) {
                System.out.println("adInfo error " + listAdvertisId);
                continue;
            }
            AdvertisementInfoData adInfoData = new AdvertisementInfoData(adInfo);
            result.add(adInfoData);
        }
    }

    public static DataResponse removeAdvertisement(int partnerId, long advertisementId) {
        if (!Partner2AdListClient.instance().existed(partnerId, advertisementId)) {
            System.out.println("error remove advertesement of orther partner " + partnerId + " adId " + advertisementId);
            return DataResponse.CHECK_VALUE_INVALID;
        }
        List<Long> listBeaconId = Ad2BeaconListClient.instance().getListAll(advertisementId);
        if (listBeaconId != null && listBeaconId.size() > 0) {
            for (int i = 0; i < listBeaconId.size(); i++) {
                long beaconId = listBeaconId.get(i);
                if (beaconId <= 0) {
                    continue;
                }
                Beacon2AdListClient.instance().remove(beaconId, advertisementId);
            }
        }
        Partner2AdListClient.instance().remove(partnerId, advertisementId);
        return new DataResponse(true);
    }

    public static DataResponse putBeaconInfo(String uuid, int bMajor, int bMinor, double lat, double lng, boolean enable, String name, String description, int partnerId) {
        long beaconId = BeaconModel.genBeaconId(uuid, bMajor, bMinor);
        TBeaconInfo beaconInfo = new TBeaconInfo(beaconId, uuid, bMajor, bMinor, lat, lng, enable, name, description);
        boolean result = UpBeaconClient.instance().put(beaconInfo);
        //return new DataResponse(result);
        List<Long> partnerList = Beacon2ParterListClient.instance().getListAll(beaconId);
        BeaconInfoData modelData = new BeaconInfoData(beaconInfo);
        modelData.partnerIds = partnerList;
        modelData.numberPartner = partnerList.size();
        PutBeaconData putData = new PutBeaconData(result, modelData);
        if (beaconId > 0) {
            BeaconIdListClient.instance().put(AppConfig.mBeaconBossId, beaconId);
            BeaconLocationClient.instance().add(lat, lng, beaconId);
            Partner2BeaconListClient.instance().put(partnerId, beaconId);
        }
        return new DataResponse(putData);
    }
      public static DataResponse doRemoveBeacon(long beaconId, int ownerId) {
        boolean result = UpBeaconClient.instance().remove(beaconId);
        if(result) {
            BeaconIdListClient.instance().remove(AppConfig.mBeaconBossId, beaconId);
            BeaconLocationClient.instance().add(0, 0, beaconId);
            List<Long> partnerList = Beacon2ParterListClient.instance().getListAll(beaconId);
            if(partnerList != null && partnerList.size() >0) {
                for (Long partnerId : partnerList) {
                    BeaconModel.doRemovePartnerFromBeacon(beaconId, partnerId);
                }
            }
            BeaconModel.doRemovePartnerFromBeacon(beaconId, ownerId);
            Partner2BeaconListClient.instance().remove(ownerId, beaconId);
        }
        return new DataResponse(result);
    }
    
}
