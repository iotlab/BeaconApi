/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.models;

import configuration.AppConfig;
import java.util.ArrayList;
import java.util.List;
import service.response.DataResponse;
import sns.backend.transport.UpAdvertisementClient;
import vng.up.beacon.thrift.TAdvertisementInfo;
import service.models.data.AdvertisementInfoData;
import service.models.data.AdvertisementInfoDataList;
import sns.backend.transport.Beacon2AdListClient;
import sns.backend.transport.BeaconIdListClient;

/**
 *
 * @author prophet
 */
public class AdvertisementModel {

    public static DataResponse addAdvertisement(String title, String content, long startTime, long endTime, boolean enable) {
        System.out.println("addAdvertisement title: " + title + " content: " + content + "startTime: " + startTime + " endTime: " + endTime);
        long adId = IdGeneratorModel.genAdvertisementId();
        System.out.println("adId: " + adId);
        if (adId > 0) {
            TAdvertisementInfo adInfo = new TAdvertisementInfo(adId, title, content, startTime, endTime, enable);
            boolean result = UpAdvertisementClient.instance().put(adInfo);
            if (result) {
                BeaconIdListClient.instance().put(AppConfig.mAdvertisementBossId, adId);
                AdvertisementInfoData adInfoData = new AdvertisementInfoData(adInfo);
                AdvertisementInfoData.AdvertisementInfoResponse addResponse = new AdvertisementInfoData.AdvertisementInfoResponse(result, adInfoData);
                return new DataResponse(addResponse);
            } else {
                AdvertisementInfoData.AdvertisementInfoResponse addResponse = new AdvertisementInfoData.AdvertisementInfoResponse(result, null);
                return new DataResponse(addResponse);
            }
        }
        return DataResponse.PARAM_ERROR;
    }
    
    public static DataResponse updateAdvertisement(long adId, String title, String content, long startTime, long endTime, boolean enable) {
        if (adId > 0) {
            TAdvertisementInfo adInfo = new TAdvertisementInfo(adId, title, content, startTime, endTime, enable);
            boolean result = UpAdvertisementClient.instance().put(adInfo);
            if (result) {
                AdvertisementInfoData adInfoData = new AdvertisementInfoData(adInfo);
                AdvertisementInfoData.AdvertisementInfoResponse addResponse = new AdvertisementInfoData.AdvertisementInfoResponse(result, adInfoData);
                return new DataResponse(addResponse);
            } else {
                AdvertisementInfoData.AdvertisementInfoResponse addResponse = new AdvertisementInfoData.AdvertisementInfoResponse(result, null);
                return new DataResponse(addResponse);
            }
        }
        return DataResponse.PARAM_ERROR;
    }

    public static DataResponse getAdvertisement(long adId) {
        if (adId > 0) {
            TAdvertisementInfo adInfo = UpAdvertisementClient.instance().get(adId);
            if (adInfo != null) {
                AdvertisementInfoData adInfoData = new AdvertisementInfoData(adInfo);
                return new DataResponse(adInfoData);
            }
        }
        return DataResponse.PARAM_ERROR;
    }
    
    public static DataResponse getAdvertisementInfoList(int start, int length) {
        List<Long> adIdList = BeaconIdListClient.instance().getSlice(AppConfig.mAdvertisementBossId, start, length);
        if(adIdList != null) {
            List<TAdvertisementInfo> adInfoList = UpAdvertisementClient.instance().getAdvertisementInfoList(adIdList);
            if(adInfoList == null)
                return DataResponse.PARAM_ERROR;
            List<AdvertisementInfoData> adModelList = new ArrayList<AdvertisementInfoData>();
            for(int i = 0; i< adInfoList.size(); i++) {
                TAdvertisementInfo adInfo = adInfoList.get(i);
                AdvertisementInfoData adInfoData = new AdvertisementInfoData(adInfo);
//                int numberBeacon = Partner2BeaconListClient.instance().count(partnerInfo.partnerId);
//                partnerInfoData.numberBeacon = numberBeacon;
                adModelList.add(adInfoData);
            }
            int total = BeaconIdListClient.instance().count(AppConfig.mAdvertisementBossId);
            AdvertisementInfoDataList returnList = new AdvertisementInfoDataList(total, adModelList);
            return new DataResponse(returnList);
        }
        return DataResponse.PARAM_ERROR;
    }
    
    public static DataResponse removeAdvertisement(long adId) {
        if (adId > 0) {
            boolean result = UpAdvertisementClient.instance().remove(adId);
            if(result) {
                BeaconIdListClient.instance().remove(AppConfig.mAdvertisementBossId, adId);
            }
            return new DataResponse(result);
        }
        return DataResponse.PARAM_ERROR;
    }
    
    public static DataResponse getAdvertisementsFromBeacon(long beaconId) {
        if (beaconId > 0) {
            List<Long> adIdList = Beacon2AdListClient.instance().getListAll(beaconId);
            if(adIdList != null) {
                List<TAdvertisementInfo> adInfoList = UpAdvertisementClient.instance().getAdvertisementInfoList(adIdList);
                List<AdvertisementInfoData> adInfoDataList = new ArrayList<AdvertisementInfoData>();
                for(int i = 0; i< adInfoList.size(); i++) {
                    TAdvertisementInfo adInfo = adInfoList.get(i);
                    if(adInfo.advertisementId >0) {
                        AdvertisementInfoData adInfoData = new AdvertisementInfoData(adInfo);
                        adInfoDataList.add(adInfoData);
                    } else {
                        // can remove ra khoi danh sach
                        if(i < adIdList.size()) {
                            Beacon2AdListClient.instance().removeAt(beaconId, i);
                        }
                    }
                }
                return new DataResponse(adInfoDataList);
            }
            
        }
        return DataResponse.PARAM_ERROR;
    }
    
    public static DataResponse addAdsToBeacon(long beaconId, String adIdsStr) {
        String[] adIdArr = adIdsStr.split(",");
        if(adIdArr.length >0) {
            List<Long> adIdList = new ArrayList<Long>();
            for(int i = 0; i< adIdArr.length; i++) {
                String adIdStr = adIdArr[i];
                long adId = -1;
                try{
                    adId = Long.parseLong(adIdStr);
                }catch(Exception ex) {
                    
                }
                if(adId >0) {
                    TAdvertisementInfo adInfo = UpAdvertisementClient.instance().get(adId);
                    if(adInfo.advertisementId >0)
                        adIdList.add(adId);
                }
            }
            if(adIdList.size() >0) {
                Beacon2AdListClient.instance().putList(beaconId, adIdList);
            }
            return new DataResponse(true);
        }
        return DataResponse.PARAM_ERROR;
    }
    
    public static DataResponse removeAdsFromBeacon(long beaconId, String adIdsStr) {
        String[] adIdArr = adIdsStr.split(",");
        if(adIdArr.length >0) {
            List<Long> adIdList = new ArrayList<Long>();
            for(int i = 0; i< adIdArr.length; i++) {
                String adIdStr = adIdArr[i];
                long adId = -1;
                try{
                    adId = Long.parseLong(adIdStr);
                }catch(Exception ex) {
                    
                }
                if(adId >0) {
                    adIdList.add(adId);
                }
            }
            if(adIdList.size() >0) {
                Beacon2AdListClient.instance().removeList(beaconId, adIdList);
            }
            return new DataResponse(true);
        }
        return DataResponse.PARAM_ERROR;
    }
    
    public static DataResponse removeAllAdsFromBeacon(long beaconId) {
        if (beaconId > 0) {
            List<Long> adIdList = Beacon2AdListClient.instance().getListAll(beaconId);
            if(adIdList != null&& !adIdList.isEmpty()) {
                Beacon2AdListClient.instance().removeList(beaconId, adIdList);
                return new DataResponse(true);
            }
            return new DataResponse(false);
        }
        return DataResponse.PARAM_ERROR;
    }
}
