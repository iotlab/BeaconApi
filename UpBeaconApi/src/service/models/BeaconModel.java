/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.models;

import configuration.AppConfig;
import java.util.ArrayList;
import java.util.List;
import service.models.data.BeaconInfoData;
import service.models.data.BeaconInfoDataList;
import service.models.data.PutBeaconData;
import service.response.DataResponse;
import sns.backend.transport.Beacon2DeviceViewAdListClient;
import sns.backend.transport.Beacon2ParterListClient;
import sns.backend.transport.BeaconIdListClient;
import sns.backend.transport.BeaconName2IdClient;
import sns.backend.transport.Partner2BeaconListClient;
import sns.backend.transport.UpBeaconClient;
import sns.backend.transport.UpBeaconPartnerClient;
import vng.up.beacon.thrift.TBeaconInfo;
import vng.up.beacon.thrift.TPartnerInfo;
import sns.backend.transport.BeaconLocationClient;
import sns.backend.transport.UserLocationClient;
import vng.up.beacon.location.thrift.TUserDistance;

/**
 *
 * @author prophet
 */
public class BeaconModel {
    public static long genBeaconId(String uuid, int bMajor, int bMinor) {
        //return uuid + " Major " + String.valueOf(bMajor) + " Minor " + bMinor;
        String beaconName = uuid + " Major " + String.valueOf(bMajor) + " Minor " + String.valueOf(bMinor);
        long beaconId = BeaconName2IdClient.instance().get(beaconName);
        if(beaconId >0)
            return beaconId;
        beaconId = IdGeneratorModel.genBeaconId(beaconName);
        return beaconId;
    }
    public static DataResponse putBeaconInfo(String uuid, int bMajor, int bMinor, double lat, double lng, boolean enable, String name, String description) {
        long beaconId = genBeaconId(uuid, bMajor, bMinor);
        TBeaconInfo beaconInfo = new TBeaconInfo(beaconId, uuid, bMajor, bMinor, lat, lng, enable, name, description);
        boolean result = UpBeaconClient.instance().put(beaconInfo);
        //return new DataResponse(result);
        List<Long> partnerList = Beacon2ParterListClient.instance().getListAll(beaconId);
        BeaconInfoData modelData = new BeaconInfoData(beaconInfo);
        modelData.partnerIds = partnerList;
        modelData.numberPartner = partnerList.size();
        PutBeaconData putData = new PutBeaconData(result, modelData);
        
        if(beaconId >0) {
            BeaconIdListClient.instance().put(AppConfig.mBeaconBossId, beaconId);
            BeaconLocationClient.instance().add(lat, lng, beaconId);
        }
        return new DataResponse(putData);
//        return DataResponse.PARAM_ERROR;
    }
    
    public static DataResponse doAddPartnerToBeacon(long beaconId, long partnerId) {
        TBeaconInfo beaconInfo = UpBeaconClient.instance().getBeaconInfo(beaconId);
        if(beaconInfo == null || beaconInfo.beaconId != beaconId) {
            return new DataResponse(1100, "do not exist beacon");
        }
        TPartnerInfo partnerInfo = UpBeaconPartnerClient.instance().get(partnerId);
        if(partnerInfo == null || partnerInfo.partnerId != partnerId) {
            return new DataResponse(1101, "do not exist partner");
        }
        boolean result = Beacon2ParterListClient.instance().insert(beaconId, partnerId);
        if(result) {
            result = Partner2BeaconListClient.instance().insert(partnerId, beaconId);
        }
        return new DataResponse(result);
    }
    
    public static DataResponse doAddPartnersToBeacon(long beaconId, String partnerIds) {
        TBeaconInfo beaconInfo = UpBeaconClient.instance().getBeaconInfo(beaconId);
        if (beaconInfo == null || beaconInfo.beaconId != beaconId) {
            return new DataResponse(1100, "do not exist beacon");
        }
        String[] partnerIdArr = partnerIds.split(",");
        if(partnerIdArr != null && partnerIdArr.length >0) {
            List<Long> partnerIdList = new ArrayList<Long>();
            for(int i = 0; i< partnerIdArr.length; i++) {
                String partnerIdStr = partnerIdArr[i];
                try{
                    long partnerId = Long.parseLong(partnerIdStr);
                    if(partnerId >0) {
                        TPartnerInfo partnerInfo = UpBeaconPartnerClient.instance().get(partnerId);
                        if(partnerInfo != null && partnerInfo.partnerId == partnerId) {
                            partnerIdList.add(partnerId);
                        }
                        
                    }
                }catch(Exception ex) {
                    
                }
            }
            if(partnerIdList.size() >0) {
                boolean result = Beacon2ParterListClient.instance().putList(beaconId, partnerIdList);
                for(int i = 0; i < partnerIdList.size(); i++) {
                    long partnerId = partnerIdList.get(i);
                    Partner2BeaconListClient.instance().insert(partnerId, beaconId);
                }
                return new DataResponse(result);
            }
        }
        return DataResponse.PARAM_ERROR;
    }
    
    public static DataResponse doAddBeaconsToPartner(long partnerId, String beaconIds) {
        TPartnerInfo partnerInfo = UpBeaconPartnerClient.instance().get(partnerId);
        if (partnerInfo == null || partnerInfo.partnerId != partnerId) {
            return new DataResponse(1100, "do not exist partner");
        }
        String[] beaconIdArr = beaconIds.split(",");
        if(beaconIdArr != null && beaconIdArr.length >0) {
            List<Long> beaconIdList = new ArrayList<Long>();
            for(int i = 0; i< beaconIdArr.length; i++) {
                String beaconIdStr = beaconIdArr[i];
                try{
                    long beaconId = Long.parseLong(beaconIdStr);
                    if(beaconId >0) {
                        TBeaconInfo beaconInfo = UpBeaconClient.instance().getBeaconInfo(beaconId);
                        if(beaconInfo != null && beaconInfo.beaconId == beaconId) {
                            beaconIdList.add(beaconId);
                        }
                        
                    }
                }catch(Exception ex) {
                    
                }
            }
            if(beaconIdList.size() >0) {
                boolean result = Partner2BeaconListClient.instance().putList(partnerId, beaconIdList);
                for(int i = 0; i < beaconIdList.size(); i++) {
                    long beaconId = beaconIdList.get(i);
                    Beacon2ParterListClient.instance().insert(beaconId, partnerId);
                }
                return new DataResponse(result);
            }
        }
        return DataResponse.PARAM_ERROR;
    }
    
    public static DataResponse doRemovePartnerFromBeacon(long beaconId, long partnerId) {
        boolean result = Beacon2ParterListClient.instance().remove(beaconId, partnerId);
        if(result) {
            result = Partner2BeaconListClient.instance().remove(partnerId, beaconId);
        }
        return new DataResponse(result);
    }
    
    public static DataResponse getBeaconInfo(long beaconId) {
        TBeaconInfo beaconInfo = UpBeaconClient.instance().getBeaconInfo(beaconId);
        if(beaconInfo == null)
            return DataResponse.PARAM_ERROR;
        else {
            List<Long> partnerList = Beacon2ParterListClient.instance().getListAll(beaconId);
            BeaconInfoData modelData = new BeaconInfoData(beaconInfo);
            modelData.partnerIds = partnerList;
            modelData.numberPartner = partnerList.size();
            List<Long> nearUserList = UserLocationClient.instance().searchUsers(beaconInfo.lat, beaconInfo.lng, AppConfig.USER_DISTANCE_RADIUS, 0, 50);
            if(nearUserList != null) {
                modelData.nearUsers = nearUserList.size();
            }
            modelData.usersViewAd = Beacon2DeviceViewAdListClient.instance().count(beaconId);
            return new DataResponse(modelData);
        }
    }

    public static DataResponse getBeaconList(int start, int length) {
        List<Long> beaconIdList = BeaconIdListClient.instance().getSlice(AppConfig.mBeaconBossId, start, length);
        if (beaconIdList != null) {
            List<TBeaconInfo> beaconInfoList = UpBeaconClient.instance().getBeaconInfoList(beaconIdList);
            if (beaconInfoList == null) {
                return DataResponse.PARAM_ERROR;
            } else {
                List<BeaconInfoData> modelDataList = new ArrayList<BeaconInfoData>();
                for(int i = 0; i< beaconInfoList.size(); i++) {
                    TBeaconInfo beaconInfo = beaconInfoList.get(i);
                    if(beaconInfo != null && beaconInfo.beaconId > 0) {
                        List<Long> partnerList = Beacon2ParterListClient.instance().getListAll(beaconInfo.beaconId);
                        BeaconInfoData modelData = new BeaconInfoData(beaconInfo);
                        modelData.partnerIds = partnerList;
                        modelData.numberPartner = partnerList.size();
                        System.out.println("lat: " + beaconInfo.lat + " longtitude: " + beaconInfo.lng);
                        List<Long> nearUserList = UserLocationClient.instance().searchUsers(beaconInfo.lat, beaconInfo.lng, AppConfig.USER_DISTANCE_RADIUS, 0, 50);
                        List<TUserDistance> userDistanceList = UserLocationClient.instance().searchUsersWithDistance(beaconInfo.lat, beaconInfo.lng, AppConfig.USER_DISTANCE_RADIUS, 0, 50);
                        if (userDistanceList != null) {
                            System.out.println("so device: " + userDistanceList.size());
                        }
                        if (nearUserList != null) {
                            for(int j = 0; j < nearUserList.size(); j++) {
                                System.out.println("deviceId: " + nearUserList.get(j));
                            }
                            modelData.nearUsers = nearUserList.size();
                        }
                        modelData.usersViewAd = Beacon2DeviceViewAdListClient.instance().count(beaconInfo.beaconId);
                        modelDataList.add(modelData);
                    }
                }
                int total = BeaconIdListClient.instance().count(AppConfig.mBeaconBossId);
                System.out.println("total: " + total);
                BeaconInfoDataList beaconInfoDataList = new BeaconInfoDataList(total, modelDataList);
                return new DataResponse(beaconInfoDataList);
            }
        }
        return DataResponse.PARAM_ERROR;
    }
    
    public static DataResponse getBeaconListFromPartner(long partnerId, int start, int length) {
        //List<Long> beaconIdList = BeaconIdListClient.instance().getSlice(AppConfig.mBeaconBossId, start, length);
        List<Long> beaconIdList = Partner2BeaconListClient.instance().getSlice(partnerId, start, length);
        if (beaconIdList != null) {
            List<TBeaconInfo> beaconInfoList = UpBeaconClient.instance().getBeaconInfoList(beaconIdList);
            if (beaconInfoList == null) {
                return DataResponse.PARAM_ERROR;
            } else {
                List<BeaconInfoData> modelDataList = new ArrayList<BeaconInfoData>();
                for(int i = 0; i< beaconInfoList.size(); i++) {
                    TBeaconInfo beaconInfo = beaconInfoList.get(i);
                    if(beaconInfo != null && beaconInfo.beaconId > 0) {
                        List<Long> partnerList = Beacon2ParterListClient.instance().getListAll(beaconInfo.beaconId);
                        BeaconInfoData modelData = new BeaconInfoData(beaconInfo);
                        modelData.partnerIds = partnerList;
                        modelData.numberPartner = partnerList.size();
                        modelDataList.add(modelData);
                    }
                }
                int total = Partner2BeaconListClient.instance().count(partnerId);
                System.out.println("total: " + total);
                BeaconInfoDataList beaconInfoDataList = new BeaconInfoDataList(total, modelDataList);
                return new DataResponse(beaconInfoDataList);
            }
        }
        return DataResponse.PARAM_ERROR;
    }
    
    public static DataResponse doRemoveBeacon(long beaconId) {
        boolean result = UpBeaconClient.instance().remove(beaconId);
        if(result) {
            BeaconIdListClient.instance().remove(AppConfig.mBeaconBossId, beaconId);
            BeaconLocationClient.instance().add(0, 0, beaconId);
            
            List<Long> partnerList = Beacon2ParterListClient.instance().getListAll(beaconId);
            if(partnerList != null && partnerList.size() >0) {
                for (Long partnerId : partnerList) {
                    doRemovePartnerFromBeacon(beaconId, partnerId);
                }
            }
        }
        return new DataResponse(result);
    }
    
    
    public static DataResponse updateLocation(long beaconId, double lat, double lng) {
        boolean result = UpBeaconClient.instance().updateLocation(beaconId, lat, lng);
        if(result) {
            BeaconLocationClient.instance().add(lat, lng, beaconId);
        }
        return new DataResponse(result);
    }
    
    public static DataResponse updateState(long beaconId, boolean enable) {
        boolean result = UpBeaconClient.instance().updateState(beaconId, enable);
        return new DataResponse(result);
    }
}
