/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.models.data;

import java.util.List;
import vng.up.beacon.thrift.TBeaconInfo;

/**
 *
 * @author prophet
 */
public class BeaconInfoData {

    public long beaconId; // required
    public String uuid; // required
    public int bMajor; // required
    public int bMinor; // required
    public double lat; // required
    public double lng; // required
    //public boolean enable; // required
    public int enable; // required
    public List<Long> partnerIds;
    public int numberPartner;
    public String beaconName; // required
    public String description;
    public int nearUsers;
    public int usersViewAd;

    public BeaconInfoData(TBeaconInfo beaconInfo) {
        this.beaconId = beaconInfo.beaconId;
        this.uuid = beaconInfo.uuid;
        this.bMajor = beaconInfo.bMajor;
        this.bMinor = beaconInfo.bMinor;
        this.lat = beaconInfo.lat;
        this.lng = beaconInfo.lng;
        this.enable = beaconInfo.enable ? 1 : 0;
        this.beaconName = beaconInfo.beaconName;
        this.description = beaconInfo.description;
    }
}
