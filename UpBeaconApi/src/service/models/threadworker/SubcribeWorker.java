/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service.models.threadworker;

//import NotifyBus.Message;
//import sns.backend.transport.notifybus.NotifyBusClient;

/**
 *
 * @author nhungptt
 */
public class SubcribeWorker {

	private static SubcribeWorker _instance = new SubcribeWorker();

	public static SubcribeWorker instance() {
		return _instance;
	}

	public void pushNotifyBus(String topic, String data) {
		SubscribeNotifyBusWorker worker = new SubscribeNotifyBusWorker(topic, data);
		BackgroundExecutor.get().submit(worker);
	}

	public class SubscribeNotifyBusWorker implements Runnable {

		public SubscribeNotifyBusWorker(String topic, String data) {
			_topic = topic;
			_data = data;
		}

//		public SubscribeNotifyBusWorker(Message msg) {
//			_topic = msg.topic;
//			_data = msg.data;
//		}

		@Override
		public void run() {
			//NotifyBusClient.instance().pushSimpleMessage(_topic, _data);
		}
		private String _topic;
		private String _data;
	}
}
