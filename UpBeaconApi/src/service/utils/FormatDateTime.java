package service.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FormatDateTime {

    private static final Logger logger = LoggerFactory.getLogger(FormatDateTime.class);
    private static final String EMPTY_STRING = "";
    private static final String STR_DATE = "%s:%s %s %s";
    private static final String STR_DATE_LITE = "%s:%s %s";
    private static final String STR_CACH_DAY = "%s %s";
    private static final int SHOW_CACH_DAY = 43200;
    private static final String TODAY = "hôm nay";
    private static final String YESTERDAY = "hôm qua";
    private static HashMap<String, String> dayInWeek = null;

    static {
        dayInWeek = new HashMap<String, String>();
        dayInWeek.put("Sun", "Chủ nhật");
        dayInWeek.put("Mon", "Thứ hai");
        dayInWeek.put("Tue", "Thứ ba");
        dayInWeek.put("Wed", "Thứ tư");
        dayInWeek.put("Thu", "Thứ năm");
        dayInWeek.put("Fri", "Thứ sáu");
        dayInWeek.put("Sat", "Thứ bảy");
    }

    public static String formatDateTime(Date d) {
        int ts = (int) (d.getTime() / 1000);
        return formatDateTime(ts);
    }

    public static String formatDateTime(long time, boolean isMilisecondTimeStamp) {
        if (isMilisecondTimeStamp == true) {
            int ts = (int) (time / 1000);
            return formatDateTime(ts);
        } else {
            int ts = (int) time;
            return formatDateTime(ts);
        }
    }

    /*
     * time: timestamp in second
     */
    public static String formatDateTimeFull(int time) {
        if (time == 0) {
            return EMPTY_STRING;
        }

        int curTime = (int) (System.currentTimeMillis() / 1000);
        int cachday = curTime - time;

        if (cachday <= SHOW_CACH_DAY) {
            // cach giay
            if (cachday >= 0 && cachday <= 60) {
                return String.format(STR_CACH_DAY, cachday, "giây trước");
            } else if (cachday >= 61 && cachday <= 3600) {
                int phut = Math.round(cachday / 60);
                return String.format(STR_CACH_DAY, phut, "phút trước");
            } else if (cachday >= 3601 && cachday <= 43200) {
                int tieng = Math.round(cachday / 3600);
                return String.format(STR_CACH_DAY, tieng, "tiếng trước");
            }
        }

        Date curDate = new Date(System.currentTimeMillis());
        Date userDate = new Date((long) time * 1000);
        SimpleDateFormat sdf1 = new SimpleDateFormat("w");
        int systemWeek = Integer.parseInt(sdf1.format(curDate));
        int userWeek = Integer.parseInt(sdf1.format(userDate));

        SimpleDateFormat sdf2 = new SimpleDateFormat("D");
        int systemDay = Integer.parseInt(sdf2.format(curDate));
        int userDay = Integer.parseInt(sdf2.format(userDate));

        int systemYear = curDate.getYear();
        int userYear = userDate.getYear();

        SimpleDateFormat sdf3 = new SimpleDateFormat("Hm");
        int buoi = Integer.parseInt(sdf3.format(userDate));
        String strBuoi = "";
        if (buoi >= 0 && buoi <= 1059) {
            strBuoi = "sáng";
        } else if (buoi >= 1100 && buoi <= 1359) {
            strBuoi = "trưa";
        } else if (buoi >= 1400 && buoi <= 1859) {
            strBuoi = "chiều";
        } else {
            strBuoi = "tối";
        }

        sdf1 = new SimpleDateFormat("h");
        sdf2 = new SimpleDateFormat("m");
        if (userYear == systemYear) {
            if (userDay == systemDay) {
                return String.format(STR_DATE, sdf1.format(userDate), sdf2.format(userDate), strBuoi, TODAY);
            } else if (systemDay - userDay == 1) {
                return String.format(STR_DATE, sdf1.format(userDate), sdf2.format(userDate), strBuoi, YESTERDAY);
            }
            if (userWeek == systemWeek) {
                sdf3 = new SimpleDateFormat("E");
                return String.format(STR_DATE, sdf1.format(userDate), sdf2.format(userDate), strBuoi, dayInWeek.get(sdf3.format(userDate)));
            } else if (cachday >= 86400 && cachday <= 2592000) {
                int ngay = Math.round(cachday / 86400);
                return String.format(STR_CACH_DAY, ngay, "ngày trước");
            } else if (cachday >= 2592000 && cachday <= 31104000) {
                int thang = Math.round(cachday / 2592000);
                return String.format(STR_CACH_DAY, thang, "tháng trước");
            } else {
                sdf3 = new SimpleDateFormat("dd/MM/yyyy");
                return String.format(STR_DATE, sdf1.format(userDate), sdf2.format(userDate), strBuoi, sdf3.format(userDate));
            }
        } else {
            sdf3 = new SimpleDateFormat("dd/MM/yyyy");
            return String.format(STR_DATE, sdf1.format(userDate), sdf2.format(userDate), strBuoi, sdf3.format(userDate));
        }
    }

    /*
     * time: timestamp in second
     */
    public static String formatDateTime(int time) {
        if (time == 0) {
            return EMPTY_STRING;
        }

        int curTime = (int) (System.currentTimeMillis() / 1000);
        int cachday = curTime - time;

        if (cachday <= SHOW_CACH_DAY) {
            // cach giay
            if (cachday >= 0 && cachday <= 60) {
                return String.format(STR_CACH_DAY, cachday, "giây trước");
            } else if (cachday >= 61 && cachday <= 3600) {
                int phut = Math.round(cachday / 60);
                return String.format(STR_CACH_DAY, phut, "phút trước");
            } else if (cachday >= 3601 && cachday <= 43200) {
                int tieng = Math.round(cachday / 3600);
                return String.format(STR_CACH_DAY, tieng, "tiếng trước");
            }
        }

        Date curDate = new Date(System.currentTimeMillis());
        Date userDate = new Date((long) time * 1000);
        SimpleDateFormat sdf1 = new SimpleDateFormat("w");
        int systemWeek = Integer.parseInt(sdf1.format(curDate));
        int userWeek = Integer.parseInt(sdf1.format(userDate));

        SimpleDateFormat sdf2 = new SimpleDateFormat("D");
        int systemDay = Integer.parseInt(sdf2.format(curDate));
        int userDay = Integer.parseInt(sdf2.format(userDate));

        int systemYear = curDate.getYear();
        int userYear = userDate.getYear();

        SimpleDateFormat sdf3 = null;

        sdf1 = new SimpleDateFormat("H");
        sdf2 = new SimpleDateFormat("m");
        if (userYear == systemYear) {
            if (userDay == systemDay) {
                return String.format(STR_DATE_LITE, sdf1.format(userDate), sdf2.format(userDate), TODAY);
            } else if (systemDay - userDay == 1) {
                return String.format(STR_DATE_LITE, sdf1.format(userDate), sdf2.format(userDate), YESTERDAY);
            }
            if (userWeek == systemWeek) {
                sdf3 = new SimpleDateFormat("E");
                return String.format(STR_DATE_LITE, sdf1.format(userDate), sdf2.format(userDate), dayInWeek.get(sdf3.format(userDate)));
            } else if (cachday >= 86400 && cachday <= 2592000) {
                int ngay = Math.round(cachday / 86400);
                return String.format(STR_CACH_DAY, ngay, "ngày trước");
            } else if (cachday >= 2592000 && cachday <= 31104000) {
                int thang = Math.round(cachday / 2592000);
                return String.format(STR_CACH_DAY, thang, "tháng trước");
            } else {
                sdf3 = new SimpleDateFormat("dd/MM/yyyy");
                return String.format(STR_DATE_LITE, sdf1.format(userDate), sdf2.format(userDate), sdf3.format(userDate));
            }
        } else {
            sdf3 = new SimpleDateFormat("dd/MM/yyyy");
            return sdf3.format(userDate);
        }
    }
    private static final DateFormat toDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * default from pattern "yyyy-MM-dd HH:mm:ss" to Date2012-02-23 23:34:14
     *
     * @param str
     * @return
     */
    public static Date fromStringToDate(String str, String pattern) {
        Date result = null;
        DateFormat toDateFormat = null;
        if (pattern == null || pattern.isEmpty()) {
            toDateFormat = FormatDateTime.toDateFormat;
        } else {
            toDateFormat = new SimpleDateFormat(pattern);
        }
        try {
            result = toDateFormat.parse(str);
        } catch (ParseException e) {
            logger.error("Parse date time error : " + e.getMessage());
        }
        return result;
    }
}