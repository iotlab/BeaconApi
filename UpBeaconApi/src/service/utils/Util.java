package service.utils;

//import FwSession.SessionResult;
//import com.vng.lib.fan.compressor.CompressorFactory;
//import com.vng.lib.fan.compressor.CompressorIterface;
//import com.vng.lib.fan.serialize.SerializeFactory;
//import com.vng.lib.fan.serialize.SerializeInterface;
import com.twmacinta.util.MD5;
//import com.vng.lib.fan.auth.VNGAuth;
import com.vng.jcore.common.Config;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.Cookie;
import org.apache.log4j.Logger;

/**
 *
 * @author triettv
 */
public class Util {

    private static final Logger log = Logger.getLogger(Util.class);
    
    public static String md5(String content) {
        MD5 md5 = new MD5();
        md5.Update(content);
        String hash = md5.asHex();
        return hash;
    }

    public static String splitShortContent(String content, int size) {
        try {
            if (content == null) {
                return "";
            }
            if (content.length() <= size) {
                return content;
            }
            String substr = content.substring(0, size);
            int spaceIndex = substr.lastIndexOf(" ");

            if (spaceIndex != -1 && spaceIndex >= size - 10) {
                substr = substr.substring(0, spaceIndex);
            }
            return substr + "...";
        } catch (Exception ex) {
            log.error("Util.splitShortContent:" + ex.getMessage(), ex);
            return "";
        }
    }

    public static boolean checkViewStats(String username, String password) {
        if (username.equals(Config.getParam("view-stats", "username")) && password.equals(Config.getParam("view-stats", "password"))) {
            return true;
        }
        return false;
    }

    public static boolean isValidVietPhone(String phoneNumber)
    {
//        String sPhoneNumber = "605-8889999";
//      //String sPhoneNumber = "605-88899991";
//      //String sPhoneNumber = "605-888999A";
//      Pattern pattern = Pattern.compile("\\d{3}-\\d{7}");
      //Pattern pattern = Pattern.compile("\\d{9,20}");  
      Pattern pattern = Pattern.compile("84\\d{9,20}");  
      Matcher matcher = pattern.matcher(phoneNumber);
 
      if (matcher.matches()) {
    	  System.out.println("Phone Number Valid" + phoneNumber);
          return true;
      }
      else
      {
    	  System.out.println("Phone Number must be in the form 0XXXXXXXXX");
          return false;
      }
    }
   
    public static boolean isValidFone(String phoneNumber)
    {
//        String sPhoneNumber = "605-8889999";
//      //String sPhoneNumber = "605-88899991";
//      //String sPhoneNumber = "605-888999A";
//      Pattern pattern = Pattern.compile("\\d{3}-\\d{7}");
      //Pattern pattern = Pattern.compile("\\d{9,20}");  
      Pattern pattern = Pattern.compile("0\\d{9,20}");  
      Matcher matcher = pattern.matcher(phoneNumber);
 
      if (matcher.matches()) {
    	  System.out.println("Phone Number Valid" + phoneNumber);
          return true;
      }
      else
      {
    	  System.out.println("Phone Number must be in the form 0XXXXXXXXX");
          return false;
      }
    }
    
    public static String toVietPhone(String phoneNumber)
    {
        return toFormatPhone("84", phoneNumber);
    }
    
    private static String toFormatPhone(String code,String phoneNumber)
    {
        if(phoneNumber.charAt(0) == '0')
            {
                return code + phoneNumber.substring(1);
            }
            return phoneNumber;
//        if(isValidFone(phoneNumber))
//        {
//            if(phoneNumber.charAt(0) == '0')
//            {
//                return code + phoneNumber.substring(1);
//            }
//            return  code + phoneNumber;
//        }
//        return "";
    }
    
    private static String getCookieValue(Cookie[] cookies, String cookieName, String defaultValue) {
        if (cookies != null) {
            for (int i = 0; i < cookies.length; i++) {
                Cookie cookie = cookies[i];
                if (cookieName.equals(cookie.getName())) {
                    return (cookie.getValue());
                }
            }
        }
        return (defaultValue);
    }

    
	public static void main(String[] args)
	{
		//b39461d1de499ae79ddc6347d502bfc2
		System.out.println(	md5("841692529005"));
	}
}
